hex_string = "757c7d51.0x67667360.0x7b66737e.0x33617c7d"
# Diviser la chaîne en sous-chaînes séparées par des espaces
hex_values = hex_string.split(".0x")

# Décodez chaque sous-chaîne en hexadécimal et inversez-les
decoded_and_reversed = [bytes.fromhex(sub).decode('utf-8')[::-1] for sub in hex_values]

# Concaténez les sous-chaînes inversées
result = ''.join(decoded_and_reversed)

print(result)
