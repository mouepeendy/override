# Override
Reverse engineering challenges focused on binary exploitation

In this project:

    -Buffer overflow

    -ret2libc

    -printf format string

    -GOT overwrite

    -Shellcode injection

    -Integer overflow
